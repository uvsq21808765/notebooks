Application ligne de commande pour la prise de notes

## Fichiers
* Add.java : hériter la class `Command` et permet d'ajouter les fichiers par le récepteur
* APP.java : main méthode et réaliser toutes les opérations désirées
* Command.java : la définition pour le fondement des différents fonctionalités
* Delete.java : hériter la class `Command` et permet de supprimer les fichiers par le récepteur
* DirectoryReceiver.java : permettre lister les notes existantes et rechercher par les notes clés
* Edit.java : hériter la class `Command` et permet de rédiger les fichiers par le récepteur
* FileReceiver.java : permettre de créer un fichier de note , modifier une note sur un éditeur , supprimer une note et voir une note sur un navigateur
* ListFile.java : hériter la class `Command` et permet d'afficher les fichiers toutes les noms de fichiers sur un bon chemin et les infos détaillés du fichiers qui peut les enregistrer au format JSON
* ListFileManager.java : permettre de lister les fichiers avec ses index par ordre
* OSValidator.java : configurer dans dse différents systèmes
* Receiver.java : définition des attributs que les différents récepteurs vont appliquer
* Search.java : hériter la class `Command` et permet de trouver les fichiers par le récepteur
* View.java : hériter la class `Command` et permet de voir les fichiers par le récepteur
* logo.txt : le déssin de 'Notebooks'
* menu.txt : interprétations des commandes
* pom.xml : configurer les plugins et les dépendences
* config.xml : configurer le path du répertoire , celui de navigateur , celui d'éditeur et le type de fichier '.adoc' etc pour Linux
* configW.xml : configurer le path du répertoire , celui de navigateur , celui d'éditeur et le type de fichier '.adoc' etc pour Windows



## Démarrer le jeu
1. Lancer 'App.java'

## Voir les instructions complètes
1. Dans l'application, saisir ** h ** pour accéder à la liste des commandes
2. Utilisez les touches fléchées pour parcourir les instructions.



Historique de la version
### 1.0.9
1. Génération du jar

### 1.0.8
1. Ajout de la fonctionnalité optionnelle pour générer le document asciidoctor présentant la liste en fusionnant les données JSON et le template.
2.Ajout du raccourci 'v ../list'

### 1.0.7
1. Vérifier si on peut ajouter un fichier qui existe déjà et rédiger des attributs asciidoctor dans ce note sur un éditeur et générer un fichier '.adoc'
2. Vérifier si on peut supprimer un fichier qui n'existe pas
3. Manager des ordres alphabétiques sur la liste des notes classés selon différents critères

### 1.0.6
1. Ajouter des raccourcis clavier pour accéder aux menus
2. Ajouter un menu pour afficher les raccourcis clavier 

### 1.0.5
1. Création des différents tests
2. Création des packages

### 1.0.4
1. Ajout des fonctionnalités Ajouter, Lister, Supprimer, Rechercher, Modifier
2. Voir des attributs asciidoctor sur un navigateur et générer un fichier '.html'
3. Rechercher des notes selon le title/projet/context
4. Utiliser la commande 'v ou view index' pour avoir la liste des notes triés selon les différents critères
4. Ajouter du Main

### 1.0.3
1. Ajout d'une option qui spécifie sur quel sytème l'on est
2. configurer le path du répertoire , celui de navigateur , celui d'éditeur et le type de fichier '.adoc' etc pour Windows
3. Ajout de cette option dans les classes utilisant le pattern command pour l'exécution

### 1.0.2
1. Ajouter d'un objet pour le fondement des différentes fonctionnalités
3. Ajouter d'un objet outil pour lire dans un fichier

### 1.0.1
1. Utilisation du pattern Command pour encapsuler un ensemble d'informations pour réaliser les actionhstelles que ajouter, modifier, supprimer, et voir sur la navigatio n web et d'un autre un pattern command pour les actions telles que recherche et la liste ds fichiers

### 1.0.0
1. Version bêta initiale
